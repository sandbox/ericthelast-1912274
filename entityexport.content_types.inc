<?php

function entityexport_ct_form($form, &$form_state) {
	$table = array(
		'#type' => 'tableselect',
		'#header' => array(t('Content Types')),
		'#options' => array(),
		'#required' => TRUE,
	);

	$types = node_type_get_types();
	foreach($types as $type_name => $type) {
		$table['#options'][$type_name] = array(
			$type_name,
		);
	}

	$form['types'] = $table;

	$form['submit'] = array(
		'#type' => 'submit',
		'#value' => t('Submit'),
	);

	return $form;
}

function entityexport_ct_form_submit(&$form, &$form_state) {
	$selected_types = array_filter($form_state['values']['types']);
	drupal_goto(request_path() . '/' . implode(',', $selected_types));
	
	echo '<pre>' . $code . '</pre>';
	drupal_exit();
}


function entityexport_ct_export($types) {
	include_once DRUPAL_ROOT . '/includes/utility.inc';
	$selected_types = explode(',', $types);
	$code = '';
	$types = node_type_get_types();
	foreach($selected_types as $type_name) {
		$type = $types[$type_name];
    if(module_exists('auto_nodetitle')) {
      $types[$type->type]->ant = array(
        'settings' => variable_get('ant_' . $type->type),
        'pattern' => variable_get('ant_pattern_' . $type->type),
        'php' => variable_get('ant_php_' . $type->type),
      );
    }
    $options = variable_get('node_options_' . $type->type, array());
    $types[$type->type]->_options = $options;
		$code .= '$types[\'' . $type->type . '\'] = ' . drupal_var_export((array) $types[$type->type]) . ";\n";
	}

	$output['code'] = array(
		'#theme' => 'textarea',
		'#value' => $code,
		'#rows' => 30,
		'#prefix' => t('Place this code in your types file:'),
	);

	$output['link'] = array(
		'#markup' => l('Back to type select', 'content-type-export'),
	);

	return $output;
}
