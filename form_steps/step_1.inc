<?php

function entityexport_form_step_1($form, &$form_state) {
	$form['entity'] = array(
		'#type' => 'tableselect',
		'#header' => array(t('Entity Type'), t('Entity')),
		'#options' => array(),
		'#empty' => t('No types found'),
		'#description' => t('Select the content type you wish to export'),
		'#multiple' => TRUE,
		'#required' => TRUE,
	);
  $entities = entity_get_info();
  foreach($entities as $entity_name => $entity) {
    if(!$entity['fieldable']) {
      continue;
    }
    foreach($entity['bundles'] as $bundle_name => $bundle) {
      $form['entity']['#options'][$entity_name . '|' . $bundle_name] = array(
        $entity['label'],
        $bundle_name,
      );
    }
  }
	
	return $form;
}
