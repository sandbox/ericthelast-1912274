<?php

function entityexport_form_step_2($form, &$form_state) {
	
	$form['fields'] = array(
		'#type' => 'tableselect',
		'#header' => array(
      t('Entity'),
      t('Bundle'),
			t('Field Name'),
			t('Field Label'),
		),
		'#options' => array(),
		'#empty' => t('No fields found'),
	);
	
  $entities = array_filter($form_state['step_information'][1]['stored_values']['entity']);
  foreach($entities as $row) {
    list($entity_name, $bundle_name) = explode('|', $row);
    $fields = field_info_instances($entity_name, $bundle_name);
    foreach($fields as $field_name => $field) {
      $form['fields']['#options'][$entity_name . '|' . $bundle_name . '|' . $field_name] = array(
        $entity_name,
        $bundle_name,
        $field_name,
        $field['label'],
      );
    }

    if(module_exists('field_group')) {
      $groups = field_group_info_groups($entity_type, $entity_name, 'form');
      foreach($groups as $group_name => $group) {
        $form['fields']['#options'][$group_name] = array(
          $group_name,
          $group->label,
        );
      }
    }
  }

	return $form;
}
