<?php

function entityexport_form_step_final($form, &$form_state) {
	include_once DRUPAL_ROOT . '/includes/utility.inc';
	$fields = array();
	$field_instances = array();
	$form_values = $form_state['step_information'][2]['stored_values'];
	$form_fields = array_filter($form_values['fields']);
	foreach($form_fields as $field) {
    list($entity_name, $bundle_name, $field_name) = explode('|', $field);
	  $field_data = field_info_field($field_name);
		$field_instance = field_info_instance($entity_name, $field_name, $bundle_name);
		unset($field_data['id']);
		unset($field_instance['id'], $field_instance['field_id']);
		$fields[$field_name] = drupal_var_export($field_data);
		$field_instances[$field] = drupal_var_export($field_instance);
	}

	echo "// Field definitions<pre>";
	foreach($fields as $field_name => $field) {
		echo "\n\n// ${field_name}\n\$fields['${field_name}'] = ";
		print_r($field);
		echo ";";
	}
	echo "\n\n// Instance definitions\n";
	foreach($field_instances as $field_name => $instance) {
		echo "\n\n// ${field_name}\n\$instances['${field_name}'] = ";
		print_r($instance);
		echo ";";
	}
	echo "</pre>";
	drupal_exit();
}
